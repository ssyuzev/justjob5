from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt',
        content_type='text/plain')),
    # url(r'^backoffice24/', include('backoffice24.urls',
    #     namespace='backoffice24')),
]


urlpatterns += i18n_patterns(
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^employee/', include('employee.urls', namespace='employee')),
    url(r'^employer/', include('employer.urls', namespace='employer')),
    url(r'^', include('main_pages.urls', namespace='main')),
)


if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.shortcuts import redirect


def is_employer(func):
    def wrap(request, *args, **kwargs):
        if request.user:
            if request.user.is_authenticated():
                profile = request.user.profile
                if profile.user_role == 'employer':
                    return func(request, *args, **kwargs)
            return redirect('employer:home')
    return wrap


def is_employee(func):
    def wrap(request, *args, **kwargs):
        if request.user:
            if request.user.is_authenticated():
                profile = request.user.profile
                if profile.user_role == 'employee':
                    return func(request, *args, **kwargs)
            return redirect('employee:home')
    return wrap

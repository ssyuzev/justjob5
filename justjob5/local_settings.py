from .settings import *

DEBUG = True

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static_files"),
)

ALLOWED_HOSTS = ['127.0.0.1', '192.168.1.5', '192.168.1.6']

INTERNAL_IPS = ['127.0.0.1']

X_FRAME_OPTIONS = 'DENY'

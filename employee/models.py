from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from employer.models.categories import JobCategory


LANG_LEVELS = (
    ('0', '----'),
    ('1', _('Beginner/Elementary')),
    ('2', _('Pre-Intermediate')),
    ('3', _('Intermediate')),
    ('4', _('Upper Intermediate')),
    ('5', _('Advanced/Fluent')),
)


LANGS = (
    ('en', _('English')),
    ('hu', _('Hungarian')),
    ('ru', _('Russian')),
    ('uk', _('Ukrainian'))
)


class EmployeeProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True)
    objective = models.CharField(
        _('Job Objective'),
        max_length=70, blank=True, default='')
    job_category = models.ForeignKey(
        JobCategory, verbose_name=_('Job Category'), blank=True, null=True)
    experience = models.TextField(
        _('Experience'),
        max_length=700, blank=True, default='')
    education = models.TextField(
        _('Education'),
        max_length=700, blank=True, default='')
    about_me = models.TextField(
        _('About Me'),
        max_length=700, blank=True, default='')
    level_of_en = models.CharField(
        _('Level of English language'),
        max_length=1, choices=LANG_LEVELS, default=LANG_LEVELS[0])
    level_of_hu = models.CharField(
        _('Level of Hungarian language'),
        max_length=1, choices=LANG_LEVELS, default=LANG_LEVELS[0])
    level_of_ru = models.CharField(
        _('Level of Russian language'),
        max_length=1, choices=LANG_LEVELS, default=LANG_LEVELS[0])
    level_of_ua = models.CharField(
        _('Level of Ukrainian language'),
        max_length=1, choices=LANG_LEVELS, default=LANG_LEVELS[0])
    other_langs = models.CharField(
        _('Other languages'),
        max_length=150, blank=True, default='')
    # contacts
    full_name = models.CharField(
        _('Full Name'),
        max_length=77, default='')
    phone = models.CharField(
        _('Phone'),
        max_length=15, blank=True, default='')
    location_city = models.CharField(
        _('City'),
        max_length=150, blank=True, default='')
    location_country = models.CharField(
        _('Country'),
        max_length=27, blank=True, default='UA')
    email = models.EmailField(
        _('Email'), max_length=150)
    skype = models.CharField(
        'Skype', max_length=50, blank=True, default='')
    linkedin_profile = models.CharField(
        _('Linkedin Profile'),
        max_length=100, blank=True, default='')
    photo = models.ImageField(
        _('Photo'),
        upload_to='photos/', max_length=1280, blank=True, null=True)
    # UI
    main_lang_ui = models.CharField(
        _('Main Interface language'),
        max_length=2, choices=LANGS, default='en')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return "{} - ({}/{})".format(
            self.full_name, self.user.username, self.user.profile.user_role)


class Certificate(models.Model):
    profile = models.ForeignKey(EmployeeProfile, blank=True, null=True)
    title = models.CharField(_('Title'), max_length=77, blank=True, default='')
    cert_file = models.FileField(
        _('File'), upload_to='candidate_certs/', blank=True, null=True)
    uploaded_at = models.DateField(_('Upload Date'), auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class DeleteAccRequest(models.Model):
    profile = models.ForeignKey(
        EmployeeProfile, on_delete=models.CASCADE, blank=True, null=True)
    uploaded_at = models.DateTimeField(_('Upload Date'), auto_now=True)
    canceled = models.BooleanField(default=False)

    def __str__(self):
        return self.profile.user


class Bookmark(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    vacancy_id = models.PositiveIntegerField(null=True, blank=True)
    vac_title = models.CharField(max_length=157, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} / {}".format(self.user, self.vac_title)

from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import activate

from justjob5.roles import is_employee

from employer.views.helpers import get_categories
from employer.models.vacancies import Vacancy
from employer.models.profile import EmployerProfile
from employer.models.job_applications import JobApplication, Comment

from .models import (
    EmployeeProfile, Certificate, DeleteAccRequest, Bookmark)

from .forms import (
    ContactForm,
    ProfileForm,
    UploadCertForm,
    MainLangForm,
    UploadPhotoForm,
    ResumeUploadForm,
    ResumeUpload2Form,
    MessageForm)


def home(request):
    user = request.user or ''
    if user.is_authenticated and user.profile.user_role == 'employee':
        return redirect('employee:vacancies')
    return render(request, "employee/home.html", {})


def add_resume(request):
    return render(request, "employee/add_resume.html", {})


def find_job(request):
    vacancies = Vacancy.objects.filter(status='act')[:20]
    hot_vacancies = Vacancy.objects.filter(
        status='act', is_hot=True).order_by('-updated_at')[:3]
    return render(request, "employee/find_job.html", {
        'vacancies': vacancies,
        'hot_vacancies': hot_vacancies
    })


def vacancies(request):
    categories = get_categories(request.LANGUAGE_CODE)
    vacancies = Vacancy.objects.filter(status='act')
    keywords = request.POST.get('keywords', '')
    category = request.POST.get('category', 'all')
    types = request.POST.getlist('jobType', ['any-type'])
    rates = request.POST.getlist('rate', ['any-rate'])
    if request.method == 'POST':
        if category != 'all':
            vacancies = vacancies.filter(category=category)
        if 'any-type' not in types:
            vacancies = vacancies.filter(job_type__in=types)

        if ['1', '2', '3'] == rates:
            rates = ['any-rate']
        print(rates)
        if 'any-rate' not in rates:
            if ['1', '2'] == rates:
                vacancies = vacancies.filter(salary__lt=25)
            else:
                if ['1'] == rates:
                    vacancies = vacancies.filter(salary__lte=10)
                elif ['2'] == rates:
                    vacancies = vacancies.filter(
                        Q(salary__lt=25) & Q(salary__gt=10))
            if ('3' in rates):
                if ['3'] == rates:
                    vacancies = vacancies.filter(salary__gte=25)
                if ['1', '3'] == rates:
                    vacancies = vacancies.filter(
                        Q(salary__lt=10) | Q(salary__gte=25))
                if ['2', '3'] == rates:
                    vacancies = vacancies.filter(salary__gt=10)

        vacancies = vacancies.filter(
            title__icontains=keywords).order_by('-published_at')
    return render(
        request,
        "employee/vacancies.html",
        {
            'categories': categories,
            'vacancies': vacancies,
            'keywords': keywords,
            'category': category,
            'types': types,
            'rates': rates
        })


@login_required()
@is_employee
def employers_view(request):
    keywords = request.POST.get('keywords', '')
    employers = EmployerProfile.objects.filter(is_active=True)
    if request.method == 'POST':
        employers = employers.filter(short_title__icontains=keywords)
    return render(request, "employee/restricted/employers.html", {
        'employers': employers,
        'keywords': keywords
    })


@login_required()
@is_employee
def employer(request, pk=None):
    employer = get_object_or_404(EmployerProfile, pk=pk)
    open_vacs = Vacancy.objects.filter(status='act', company=employer)
    return render(request, "employee/restricted/employer.html", {
        'employer': employer,
        'open_vacs': open_vacs
    })


@login_required()
@is_employee
def public_profile(request, pk=None):
    profile = get_object_or_404(EmployeeProfile, pk=pk)
    certs = Certificate.objects.filter(profile=profile, is_active=True)
    return render(
        request,
        "employee/restricted/public_profile.html",
        {'profile': profile, 'certificates': certs})


@login_required()
@is_employee
def profile(request):
    profile = get_object_or_404(EmployeeProfile, user=request.user.id)
    fullname = profile.full_name
    user_email = request.user.email
    photo = profile.photo or ''
    is_active = profile.is_active
    certificates = Certificate.objects.filter(profile=profile, is_active=True)
    form = ProfileForm(instance=profile)
    form2 = ContactForm(instance=profile)
    form3 = UploadCertForm()
    form4 = MainLangForm(instance=profile)
    form5 = UploadPhotoForm()
    saved_jobs = Bookmark.objects.filter(user=request.user)

    if request.method == 'POST' and request.POST.get('form_name') == 'profile':
        form = ProfileForm(request.POST, instance=profile)
        form.save(commit=False)
        form.objective = form.cleaned_data['objective']
        form.job_category = form.cleaned_data['job_category']
        form.experience = form.cleaned_data['experience']
        form.education = form.cleaned_data['education']
        form.about_me = form.cleaned_data['about_me']
        form.level_of_en = form.cleaned_data['level_of_en']
        form.level_of_hu = form.cleaned_data['level_of_hu']
        form.level_of_ru = form.cleaned_data['level_of_ru']
        form.level_of_ua = form.cleaned_data['level_of_ua']
        form.save()
        return HttpResponseRedirect('/employee/profile/#profile')

    if request.method == 'POST' and request.POST.get('form_name') == 'contact':
        form2 = ContactForm(request.POST, instance=profile)
        form2.save(commit=False)
        form2.full_name = form2.cleaned_data['full_name']
        form2.phone = form2.cleaned_data['phone']
        form2.location_city = form2.cleaned_data['location_city']
        form2.location_country = form2.cleaned_data['location_country']
        form2.email = form2.cleaned_data['email']
        form2.skype = form2.cleaned_data['skype']
        form2.linkedin_profile = form2.cleaned_data['linkedin_profile']
        form2.save()
        return HttpResponseRedirect('/employee/profile/#contacts')

    if request.method == 'POST' and request.POST.get('form_name') == 'cert':
        if request.FILES['cert_file']:
            form3 = UploadCertForm(request.POST, request.FILES)
            if form3.is_valid():
                new_cert = Certificate(
                    cert_file=request.FILES['cert_file'],
                    title=form3.cleaned_data['title'],
                    is_active=True,
                    profile=profile)
                new_cert.save()
                return HttpResponseRedirect('/employee/profile/#certs')

    if request.method == 'POST' and request.POST.get('form_name') == 'ml':
        form4 = MainLangForm(request.POST, instance=profile)
        if form4.is_valid():
            form4.save(commit=False)
            form4.main_lang_ui = form4.cleaned_data['main_lang_ui']
            try:
                activate(form4.main_lang_ui)
                print(form4.main_lang_ui)
            except:
                activate('en')
            form4.save()
        return redirect('employee:profile')

    if request.method == 'POST' and request.POST.get('form_name') == 'photo':
        if request.FILES['photo']:
            form5 = UploadPhotoForm(request.POST, request.FILES)
            if form5.is_valid():
                m = EmployeeProfile.objects.get(pk=profile.id)
                m.photo = form5.cleaned_data['photo']
                m.save()
            else:
                print(form5.errors)
            return redirect('employee:profile')

    return render(
        request,
        "employee/restricted/profile.html",
        {
            'fullname': fullname,
            'user_email': user_email,
            'photo': photo,
            'form': form,
            'form2': form2,
            'form3': form3,
            'form4': form4,
            'form5': form5,
            'certificates': certificates,
            'profile_id': profile.id,
            'is_active': is_active,
            'saved_jobs': saved_jobs
        })


@login_required()
@is_employee
def delete_cert(request, pk):
    if pk:
        candidate = get_object_or_404(EmployeeProfile, user=request.user.id)
        try:
            instance = Certificate.objects.get(pk=pk, profile=candidate)
        except:
            raise Http404
    if instance:
        Certificate.objects.filter(pk=instance.id).update(is_active=False)
    return redirect('employee:profile')


@login_required()
@is_employee
def delete_acc_request(request, pk):
    if pk:
        try:
            instance = get_object_or_404(
                EmployeeProfile, pk=pk, user=request.user.id)
        except:
            raise Http404
    if instance:
        EmployeeProfile.objects.filter(pk=instance.id).update(is_active=False)
        DeleteAccRequest.objects.create(
            profile=instance, canceled=False)
    return redirect('employee:profile')


@login_required()
@is_employee
def restore_acc_request(request, pk):
    if pk:
        try:
            instance = get_object_or_404(
                EmployeeProfile, pk=pk, user=request.user.id)
        except:
            raise Http404
    if instance:
        EmployeeProfile.objects.filter(pk=instance.id).update(is_active=True)
        DeleteAccRequest.objects.filter(
            profile=instance).update(canceled=True)
    return redirect('employee:profile')


def job_page(request, pk=None):
    vacancy = get_object_or_404(Vacancy, pk=pk)
    form = ResumeUploadForm()
    form2 = ResumeUpload2Form()
    is_submitted = is_bookmarked = ''
    if request.user.is_authenticated():
        try:
            employee = get_object_or_404(EmployeeProfile, user=request.user)
        except:
            raise Http404
        submitted = JobApplication.objects.filter(
            registered_user=employee, current_vacancy=vacancy)
        if submitted:
            is_submitted = True
        bookmark = Bookmark.objects.filter(
            user=request.user, vacancy_id=vacancy.id)
        if bookmark:
            is_bookmarked = True
    if request.method == 'POST' and request.POST.get('form_name') == 'r1':
        if request.FILES['cv_file']:
            form = ResumeUploadForm(request.POST, request.FILES)
            if form.is_valid():
                new_resume = JobApplication(
                    cv_file=request.FILES['cv_file'],
                    fullname=form.cleaned_data['fullname'],
                    email=form.cleaned_data['email'],
                    message=form.cleaned_data['message'],
                    current_vacancy=vacancy,
                    employer=vacancy.company)
                new_resume.save()
    if request.method == 'POST' and request.POST.get('form_name') == 'r2':
        form2 = ResumeUpload2Form(request.POST)
        if form2.is_valid():
            new_resume = JobApplication(
                fullname=request.user,
                email=request.user.email,
                message=form2.cleaned_data['message'],
                registered_user=employee or '',
                current_vacancy=vacancy,
                employer=vacancy.company)
            new_resume.save()
            return redirect('employee:vacancy', pk=pk)
    return render(request, "employee/job_page.html", {
        'vacancy': vacancy,
        'form': form,
        'form2': form2,
        'is_submitted': is_submitted,
        'is_bookmarked': is_bookmarked
    })


@login_required()
@is_employee
def save_job(request, action='add', vac_id=None):
    vac = get_object_or_404(Vacancy, pk=vac_id)
    if action == 'add':
        if vac_id and request.user.is_authenticated():
            vac_title = vac.title
            Bookmark.objects.update_or_create(
                user=request.user, vacancy_id=vac_id, vac_title=vac_title)
    if action == 'del':
        if vac_id and request.user.is_authenticated():
            Bookmark.objects.filter(
                user=request.user, vacancy_id=vac_id).delete()
            if vac.status != 'act':
                return redirect('employee:profile')
    return redirect('employee:vacancy', pk=vac_id)


@login_required()
@is_employee
def proposals(request):
    employee = get_object_or_404(EmployeeProfile, user=request.user)
    applications = JobApplication.objects.filter(
        registered_user=employee,
        is_deleted=False).values(
            'id',
            'current_vacancy__image',
            'current_vacancy__title',
            'employer__short_title',
            'created_at',)
    return render(request, "employee/restricted/proposals.html", {
        'applications': applications,
    })


@login_required()
@is_employee
def proposals_archive(request):
    employee = get_object_or_404(EmployeeProfile, user=request.user)
    applications = JobApplication.objects.filter(
        registered_user=employee,
        is_deleted=True).values(
            'id',
            'current_vacancy__image',
            'current_vacancy__title',
            'employer__short_title',
            'created_at',)
    return render(
        request,
        "employee/restricted/proposals_archive.html",
        {
            'applications': applications,
        })


@login_required()
@is_employee
def remove_proposal(request, pk=None):
    if pk:
        employee = get_object_or_404(EmployeeProfile, user=request.user)
        try:
            instance = get_object_or_404(
                JobApplication, pk=pk, registered_user=employee)
        except:
            raise Http404
        instance.is_deleted = True
        instance.discarded_by_candidate = True
        instance.save()
    return redirect('employee:proposals')


@login_required()
@is_employee
def proposal(request, pk=None):
    employee = get_object_or_404(EmployeeProfile, user=request.user)
    prop = get_object_or_404(
        JobApplication, pk=pk, registered_user=employee)
    messages = Comment.objects.filter(
        application=prop).order_by('created_at')
    new_message = MessageForm()
    if request.method == 'POST':
        new_message = MessageForm(request.POST)
        if new_message.is_valid():
            new_comment = Comment(
                application=prop,
                created_by=request.user,
                message=new_message.cleaned_data['message'])
            new_comment.save()
            return redirect('employee:proposal', pk=pk)
    return render(request, "employee/restricted/proposal.html", {
        'prop': prop,
        'messages': messages,
        'new_message': new_message,
    })


@login_required()
@is_employee
def proposal_refuse(request, pk=None):
    if pk and request.user.is_authenticated():
        employee = get_object_or_404(EmployeeProfile, user=request.user)
        prop = get_object_or_404(
            JobApplication, pk=pk, registered_user=employee)
        prop.discarded_by_candidate = True
        prop.save()
    return redirect('employee:proposal', pk=pk)
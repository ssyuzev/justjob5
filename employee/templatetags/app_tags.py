from django import template

from employer.models.job_applications import Comment, APP_STATUSES


register = template.Library()


@register.filter()
def get_last_comment(app_id, lang):
    try:
        last_comm = Comment.objects.filter(
            application=app_id).latest('created_at')
    except:
        return ''
    if lang == 'hu':
        return last_comm.message_hu
    return last_comm.message


@register.filter()
def get_status(status):
    for i in APP_STATUSES:
        if i[0] == status:
            return i[1]

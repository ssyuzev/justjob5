from django import template

from employee.models import LANG_LEVELS


register = template.Library()


@register.filter()
def get_lang_level(field):
    for i in LANG_LEVELS:
        if i[0] == field:
            return i[1]

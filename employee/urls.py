from django.conf.urls import url

from .views import (
    home,
    find_job,
    vacancies,
    job_page,
    add_resume,
    employers_view,
    employer,
    # restricted area
    profile,
    delete_cert,
    delete_acc_request,
    restore_acc_request,
    public_profile,
    save_job,
    proposals,
    proposals_archive,
    remove_proposal,
    proposal,
    proposal_refuse,
)

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^find-job/$', find_job, name='find_job'),
    url(r'^vacancies/$', vacancies, name='vacancies'),
    url(r'^add-resume/$', add_resume, name='add_resume'),
    url(r'^vacancy/(?P<pk>\d+)/$', job_page, name='vacancy'),
    url(r'^employers/$', employers_view, name='employers'),
    url(r'^employers/(?P<pk>\d+)/$', employer, name='employer'),

    # restricted area
    url(r'^profile/$', profile, name='profile'),
    url(r'^resume/(?P<pk>\d+)/$', public_profile, name='public_profile'),
    url(r'^profile/del-cert/(?P<pk>\d+)/$', delete_cert, name='del_cert'),
    url(r'^profile/delete/(?P<pk>\d+)/$', delete_acc_request, name='del_acc'),
    url(r'^profile/restore/(?P<pk>\d+)/$',
        restore_acc_request, name='restore_acc'),
    url(r'^bookmark/(?P<action>(add)|(del))/(?P<vac_id>\d+)/$',
        save_job, name='bookmark'),

    url(r'^proposals/$', proposals, name='proposals'),
    url(r'^proposals/archive/$', proposals_archive, name='proposals_archive'),
    url(r'^proposals/remove/(?P<pk>\d+)/$',
        remove_proposal, name='remove_proposal'),
    url(r'^proposal/refuse/(?P<pk>\d+)/$',
        proposal_refuse, name='proposal_refuse'),
    url(r'^proposal/(?P<pk>\d+)/$', proposal, name='proposal'),
]

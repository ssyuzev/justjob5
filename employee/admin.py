from django.contrib import admin

from .models import EmployeeProfile, DeleteAccRequest, Bookmark, Certificate


class DeleteAccRequestAdmin(admin.ModelAdmin):
    list_display = (
        'profile', 'uploaded_at', 'canceled')


admin.site.register(EmployeeProfile)
admin.site.register(Bookmark)
admin.site.register(Certificate)
admin.site.register(DeleteAccRequest, DeleteAccRequestAdmin)

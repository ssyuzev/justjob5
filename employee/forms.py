from django import forms

from .models import EmployeeProfile, Certificate
from employer.models.job_applications import JobApplication, Comment


class ProfileForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='profile')

    class Meta:
        model = EmployeeProfile
        fields = [
            'objective',
            'job_category',
            'experience',
            'education',
            'about_me',
            'level_of_en',
            'level_of_hu',
            'level_of_ru',
            'level_of_ua',
            'other_langs',
            'form_name']


class ContactForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='contact')

    class Meta:
        model = EmployeeProfile
        fields = [
            'full_name',
            'phone',
            'location_city',
            'location_country',
            'email',
            'skype',
            'linkedin_profile',
            'form_name']


class UploadCertForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='cert')

    class Meta:
        model = Certificate
        fields = [
            'title',
            'cert_file',
            'form_name']


class MainLangForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='ml')

    class Meta:
        model = EmployeeProfile
        fields = ['main_lang_ui', 'form_name']


class UploadPhotoForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='photo')

    class Meta:
        model = EmployeeProfile
        fields = ['photo', 'form_name']


class ResumeUploadForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='r1')

    class Meta:
        model = JobApplication
        fields = ['fullname', 'email', 'message', 'cv_file', 'current_vacancy']
        widgets = {'current_vacancy': forms.HiddenInput()}


class ResumeUpload2Form(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='r2')

    class Meta:
        model = JobApplication
        fields = ['message']


class MessageForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['message']

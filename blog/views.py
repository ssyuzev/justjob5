from django.shortcuts import render, redirect


def home(request):
    prefix = 'en'
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "blog/{}/home.html".format(prefix)
    return render(request, template, {})


def blog_post(request, slug):
    prefix = 'en'
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'

    if slug == 'fun-facts-part1':
        template = "blog/{}/fun_facts1.html".format(prefix)
    elif slug == 'fun-facts-part2':
        template = "blog/{}/fun_facts2.html".format(prefix)
    elif slug == 'hungarian-language':
        template = "blog/{}/about_lang.html".format(prefix)
    else:
        return redirect('blog:home')
    return render(request, template, {})

from django.conf.urls import url

from .views import (
    home,
    blog_post,

)

urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/$', blog_post, name="blog_post"),
    url(r'^$', home, name='home'),
]
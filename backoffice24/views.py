from django.contrib.auth import login, logout
from django.shortcuts import render, redirect
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if user.is_staff:
                return redirect('backoffice24:home')
            return redirect('main:home')
    else:
        form = AuthenticationForm()
    return render(request, "backoffice24/login.html", {'form': form})


def logout_view(request):
    logout(request)
    return redirect('main:home')


@login_required(login_url='/backoffice24/login/')
@staff_member_required()
def home(request):
    return render(request, "backoffice24/home.html", {})
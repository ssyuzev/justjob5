from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.utils.translation import activate

from .models import Contact
from .forms import SignUpForm, LoginForm, ContactForm
from employer.models.profile import EmployerProfile as E1P
from employee.models import EmployeeProfile as E2P


def home(request):
    return render(request, "main_pages/home.html", {})


def login_view(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = LoginForm(data=request.POST)
            if form.is_valid():
                user = form.get_user()
                user_role = user.profile.user_role
                login(request, user)
                if user_role == 'employer':
                    profile = get_object_or_404(E1P, user=user)
                    try:
                        activate(profile.main_lang_ui)
                    except:
                        activate('en')
                    return redirect('employer:cv_search')
                if user_role == 'employee':
                    profile = get_object_or_404(E2P, user=user)
                    try:
                        activate(profile.main_lang_ui)
                    except:
                        activate('en')
                    return redirect('employee:vacancies')
                return redirect('main:home')
        else:
            form = LoginForm()
        return render(request, "main_pages/login.html", {'form': form})
    else:
        return redirect('main:home')


def signup_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.user_role = form.cleaned_data.get('user_role')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            # After login redirect using user_role
            if user.profile.user_role == 'employer':
                return redirect('employer:cv_search')
            if user.profile.user_role == 'employee':
                return redirect('employee:find_job')
            return redirect('main:home')
    else:
        form = SignUpForm()
    return render(request, 'main_pages/signup.html', {'form': form})


def logout_view(request):
    if request.method == 'POST':
        logout(request)
    return redirect('main:home')


def browse_categories(request):
    return render(request, "main_pages/browse_categories.html", {})


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            comment = form.cleaned_data['comment']
            # finally save the object in db
            obj = Contact(name=name, email=email, comment=comment)
            obj.save()
            return HttpResponseRedirect('/contact/?submitted=True')
    else:
        form = ContactForm()
    return render(request, "main_pages/contact.html", {'form': form})


def privacy(request):
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "terms/{}/privacy.html".format(prefix)
    return render(request, template, {})


def terms(request):
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "terms/{}/terms.html".format(prefix)
    return render(request, template, {})


def usr_agr(request):
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "terms/{}/usr_agr.html".format(prefix)
    return render(request, template, {})


def cookies(request):
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "terms/{}/cookies.html".format(prefix)
    return render(request, template, {})

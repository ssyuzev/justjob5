from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from employer.models.profile import EmployerProfile
from employee.models import EmployeeProfile


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    user_role = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.user.username, self.user_role)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
    if instance.profile.user_role == 'employer':
        EmployerProfile.objects.update_or_create(user=instance)
    if instance.profile.user_role == 'employee':
        EmployeeProfile.objects.update_or_create(user=instance)


class Contact(models.Model):
    name = models.CharField(max_length=31)
    email = models.EmailField(max_length=100)
    comment = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Contact Form"
        verbose_name_plural = "Contact Forms"

    def __str__(self):
        return "{} ({})".format(self.name, self.email)

from django.conf.urls import url

from .views import (
    home,
    contact,
    login_view,
    signup_view,
    logout_view,
    privacy,
    terms,
    usr_agr,
    cookies,

)

urlpatterns = [
    url(r'^login/$', login_view, name='login'),
    url(r'^signup/$', signup_view, name='signup'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^contact/$', contact, name='contact'),

    url(r'^privacy/$', privacy, name='privacy'),
    url(r'^terms/$', terms, name='terms'),
    url(r'^user-agreement/$', usr_agr, name='usr_agr'),
    url(r'^cookies-policy/$', cookies, name='cookies'),

    url(r'^$', home, name='home'),
]

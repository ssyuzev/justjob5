from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

from .models import Contact

USER_ROLES = (('employer', 'Employer'), ('employee', 'Employee'))


class SignUpForm(UserCreationForm):
    user_role = forms.ChoiceField(widget=forms.RadioSelect, choices=USER_ROLES)
    email = forms.EmailField(max_length=250)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'user_role',)


class LoginForm(AuthenticationForm):
    pass


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'email', 'comment']

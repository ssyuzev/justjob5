from django import template

from employer.models.vacancies import CITIES, LOCATION_RADIUS
from employer.models.profile import EmployerProfile
from employer.models.prices import DURATION_LIST

from employee.models import EmployeeProfile

register = template.Library()


@register.filter()
def get_field_name(object, field):
    verbose_name = object._meta.get_field(field).verbose_name
    return verbose_name


@register.filter()
def get_central_city(field):
    for i in CITIES:
        if i[0] == field:
            return i[1]
    else:
        return None


@register.filter()
def get_location(field):
    for i in LOCATION_RADIUS:
        if i[0] == field:
            return i[1]


@register.filter()
def get_currency(field):
    return "{} €".format(field)


@register.filter()
def get_user_photo(user_id):
    employee = EmployeeProfile.objects.filter(
        user=user_id).values_list('photo', flat=True).last()
    photo2 = EmployerProfile.objects.filter(
        user=user_id).values_list('logo', flat=True).last()
    if employee:
        return employee
    else:
        return photo2


@register.filter()
def get_duration(field):
    for i in DURATION_LIST:
        if i[0] == field:
            return i[1]

from django.conf.urls import url

from .views.public import home, search_cv, post_job, recruitment
from .views.restricted import (
    cv_search,
    resume,
    my_vacancies,
    new_vacancy,
    edit_vacancy,
    vacancy_detail,
    vacancy_change_status,
    applications,
    pricing,
    profile,
    invoice,
    delete_acc_request,
    restore_acc_request,
    save_resume,
    app_details,
    app_archive,
    app_remove,
    app_refuse,

)

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^search-cv/$', search_cv, name='search_cv'),
    url(r'^add-job/$', post_job, name='post_job'),
    # restricted area
    url(r'^profile/$', profile, name='profile'),
    url(r'^invoice/(?P<num>\d+)/$', invoice, name='invoice'),

    url(r'^vacancies/new/$', new_vacancy, name='new_vacancy'),
    url(r'^vacancies/edit/(?P<pk>\d+)/$',
        edit_vacancy, name='edit_vacancy'),
    url(r'^vacancies/change-status/(?P<pk>\d+)/$',
        vacancy_change_status, name='change_status'),
    url(r'^vacancies/(?P<pk>\d+)/$', vacancy_detail, name='vacancy'),
    url(r'^my-vacancies/$', my_vacancies, name='my_vacancies'),

    url(r'^applications/(?P<vacancy_id>\d+)$',
        applications, name='applications'),
    url(r'^applications/details/(?P<pk>\d+)$',
        app_details, name='app_details'),
    url(r'^applications/archive/$', app_archive, name='app_archive'),
    url(r'^applications/remove/(?P<pk>\d+)/$',
        app_remove, name='app_remove'),
    url(r'^applications/refuse/(?P<pk>\d+)/$',
        app_refuse, name='app_refuse'),

    url(r'^cv-search/$', cv_search, name='cv_search'),
    url(r'^resume/(?P<pk>\d+)/$', resume, name='resume'),
    url(r'^pricing/$', pricing, name='pricing'),

    url(r'^profile/delete/(?P<pk>\d+)/$', delete_acc_request, name='del_acc'),
    url(r'^profile/restore/(?P<pk>\d+)/$',
        restore_acc_request, name='restore_acc'),
    url(r'^bookmark/(?P<action>(add)|(del))/(?P<pk>\d+)/$',
        save_resume, name='save_resume'),
    url(r'^recruitment/$', recruitment, name='recruitment'),
]

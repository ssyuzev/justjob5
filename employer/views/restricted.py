import operator
from functools import reduce
from django.db.models import Q

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404
from django.utils import timezone
from django.utils.translation import activate


from justjob5.roles import is_employer
from employee.models import EmployeeProfile, Certificate

from employer.models.profile import (
    EmployerProfile, DeleteAccRequest, SavedResume)
from employer.models.vacancies import Vacancy
from employer.models.job_applications import (
    JobApplication, Comment, APP_STATUSES)
from employer.models.invoices import Invoice
from employer.models.prices import CvSearchPrice, JobAddPrice

from employer.forms import (
    CompanyInfoForm,
    CompanyPersonForm,
    CompanyBillingForm,
    VacancyForm,
    MainLangForm)

from employee.forms import MessageForm

from .helpers import get_categories


@login_required()
@is_employer
def my_vacancies(request):
    employer = get_object_or_404(EmployerProfile, user=request.user)
    active_vacs = Vacancy.objects.filter(
        company=employer, status='act').order_by('-updated_at')
    active_vacs_count = active_vacs.count()
    moder_vacs = Vacancy.objects.filter(
        company=employer, status='moder').order_by('-updated_at')
    moder_vacs_count = moder_vacs.count()
    draft_vacs = Vacancy.objects.filter(
        company=employer, status='draft').order_by('-updated_at')
    draft_vacs_count = draft_vacs.count()
    archive_vacs = Vacancy.objects.filter(
        company=employer, status='arch').order_by('-updated_at')
    archive_vacs_count = archive_vacs.count()
    return render(
        request,
        "employer/restricted/my_vacancies.html",
        {
            'active_vacs': active_vacs,
            'active_vacs_count': active_vacs_count,
            'moder_vacs': moder_vacs,
            'draft_vacs': draft_vacs,
            'archive_vacs': archive_vacs,
            'moder_vacs_count': moder_vacs_count,
            'draft_vacs_count': draft_vacs_count,
            'archive_vacs_count': archive_vacs_count,

        })


@login_required()
@is_employer
def vacancy_detail(request, pk=None):
    vacancy = get_object_or_404(Vacancy, pk=pk)
    return render(
        request,
        "employer/restricted/vacancy.html",
        {
            'vacancy': vacancy
        })


@login_required()
@is_employer
def new_vacancy(request):
    if request.method == 'POST':
        form = VacancyForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            Vacancy.objects.create(
                title=form.data['title'],
                phone_for_vac=form.data['phone_for_vac'],
                email_for_vac=form.cleaned_data['email_for_vac'],
                job_duties=form.cleaned_data['job_duties'],
                job_demands=form.cleaned_data['job_demands'],
                job_conditions=form.cleaned_data['job_conditions'],
                job_description=form.cleaned_data['job_description'],
                central_city=form.cleaned_data['central_city'],
                location=form.cleaned_data['location'],
                salary=form.cleaned_data['salary'],
                work_hours=form.cleaned_data['work_hours'],
                is_hot=form.cleaned_data['is_hot'],
                is_need_hu=form.cleaned_data['is_need_hu'],
                status='draft',
                updated_by=request.user,
                job_type=form.cleaned_data['job_type'],
                category=form.cleaned_data['category'],
                company=get_object_or_404(
                    EmployerProfile, user=request.user.id),
            )
            return redirect('employer:my_vacancies')
    else:
        form = VacancyForm()
    return render(
        request,
        "employer/restricted/new_vacancy.html",
        {'form': form})


@login_required()
@is_employer
def edit_vacancy(request, pk=None):
    if pk:
        company = get_object_or_404(EmployerProfile, user=request.user.id)
        instance = Vacancy.objects.get(pk=pk, company=company)
    else:
        raise Http404
    form = VacancyForm(request.POST or None, instance=instance)
    if request.method == 'POST':
        if form.is_valid():
            form.save(commit=False)
            Vacancy.objects.filter(pk=instance.id).update(
                title=form.data['title'],
                phone_for_vac=form.data['phone_for_vac'],
                email_for_vac=form.cleaned_data['email_for_vac'],
                job_duties=form.cleaned_data['job_duties'],
                job_demands=form.cleaned_data['job_demands'],
                job_conditions=form.cleaned_data['job_conditions'],
                job_description=form.cleaned_data['job_description'],
                central_city=form.cleaned_data['central_city'],
                location=form.cleaned_data['location'],
                salary=form.cleaned_data['salary'],
                work_hours=form.cleaned_data['work_hours'],
                is_hot=form.cleaned_data['is_hot'],
                is_need_hu=form.cleaned_data['is_need_hu'],
                status='draft',
                updated_at=timezone.now(),
                updated_by=request.user,
                job_type=form.cleaned_data['job_type'],
                category=form.cleaned_data['category'],
                company=company)
            return redirect('employer:my_vacancies')
    return render(
        request,
        "employer/restricted/edit_vacancy.html",
        {'form': form, 'title': instance.title, 'vac_id': instance.id})


@login_required()
@is_employer
def vacancy_change_status(request, pk):
    if pk:
        company = get_object_or_404(EmployerProfile, user=request.user.id)
        try:
            instance = Vacancy.objects.get(pk=pk, company=company)
        except:
            raise Http404
    if instance:
        status = instance.status
        if status == 'draft' or status == 'arch':
            Vacancy.objects.filter(pk=instance.id).update(status='moder')
        else:
            Vacancy.objects.filter(pk=instance.id).update(status='draft')
    return redirect('employer:my_vacancies')


@login_required()
@is_employer
def applications(request, vacancy_id=None):
    vacancy = get_object_or_404(Vacancy, pk=vacancy_id)
    applications = JobApplication.objects.filter(current_vacancy=vacancy)
    new_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='new')
    iv_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='iv')
    oe_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='oe')
    hir_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='hir')
    arc_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='arc')
    dis_apps = JobApplication.objects.filter(
        current_vacancy=vacancy, status='dis')
    return render(
        request,
        "employer/restricted/proposals.html",
        {
            'vacancy_id': vacancy_id,
            'vacancy_title': vacancy.title,
            'applications': applications,
            'new_apps': new_apps,
            'iv_apps': iv_apps,
            'oe_apps': oe_apps,
            'hir_apps': hir_apps,
            'arc_apps': arc_apps,
            'dis_apps': dis_apps,
            'new_apps_c': new_apps.count(),
            'iv_apps_c': iv_apps.count(),
            'oe_apps_c': oe_apps.count(),
            'hir_apps_c': hir_apps.count(),
            'arc_apps_c': arc_apps.count(),
            'dis_apps_c': dis_apps.count(),
        })


@login_required()
@is_employer
def app_details(request, pk=None):
    employer = get_object_or_404(EmployerProfile, user=request.user)
    prop = get_object_or_404(
        JobApplication, pk=pk, employer=employer)
    messages = Comment.objects.filter(
        application=prop).order_by('created_at')
    status_list = APP_STATUSES
    new_message = MessageForm()
    if request.method == 'POST' and request.POST.get('form_name') == 'send':
        new_message = MessageForm(request.POST)
        if new_message.is_valid():
            new_comment = Comment(
                application=prop,
                created_by=request.user,
                message=new_message.cleaned_data['message'])
            new_comment.save()
            return redirect('employer:app_details', pk=pk)
    if request.method == 'POST' and\
            request.POST.get('form_name') == 'change-status':
        cur_status = request.POST.get('status')
        prop.status = cur_status
        if cur_status == 'dis':
            prop.discarded_by_employer = True
        prop.save()
        return redirect('employer:app_details', pk=pk)

    return render(request, "employer/restricted/app_page.html", {
        'prop': prop,
        'messages': messages,
        'new_message': new_message,
        'status_list': status_list
    })


@login_required()
@is_employer
def app_refuse(request, pk=None):
    if pk and request.user.is_authenticated():
        employer = get_object_or_404(EmployerProfile, user=request.user)
        prop = get_object_or_404(
            JobApplication, pk=pk, employer=employer)
        prop.discarded_by_employer = True
        prop.status = 'arc'
        prop.save()
    return redirect('employer:app_details', pk=pk)


@login_required()
@is_employer
def app_archive(request, pk=None):
    pass


@login_required()
@is_employer
def app_remove(request, pk=None):
    pass


@login_required()
@is_employer
def profile(request):
    profile = get_object_or_404(EmployerProfile, user=request.user.id)
    logo = profile.logo or ''
    invoices = Invoice.objects.filter(invoiced_to=profile)
    form = CompanyInfoForm(instance=profile)
    form2 = CompanyPersonForm(instance=profile)
    form3 = CompanyBillingForm(instance=profile)
    form4 = MainLangForm(instance=profile)
    saved_resume = SavedResume.objects.filter(user=request.user)
    if request.method == 'POST' and request.POST.get('form_name') == 'info':
        form = CompanyInfoForm(request.POST, request.FILES, instance=profile)
        form.save(commit=False)
        form.short_title = form.cleaned_data['short_title']
        form.company_phones = form.cleaned_data['company_phones']
        form.postal_address = form.cleaned_data['postal_address']
        form.about_company = form.cleaned_data['about_company']
        form.website = form.cleaned_data['website']
        form.company_email = form.cleaned_data['company_email']
        form.linkedin_url = form.cleaned_data['linkedin_url']
        form.save()
        return HttpResponseRedirect('/employer/profile/#companyInfo')

    if request.method == 'POST' and request.POST.get('form_name') == 'person':
        form2 = CompanyPersonForm(request.POST, instance=profile)
        form2.save(commit=False)
        form2.person_fullname = form2.cleaned_data['person_fullname']
        form2.person_position = form2.cleaned_data['person_position']
        form2.person_phone = form2.cleaned_data['person_phone']
        form2.person_email = form2.cleaned_data['person_email']
        form2.save()
        return HttpResponseRedirect('/employer/profile/#contactPerson')

    if request.method == 'POST' and request.POST.get('form_name') == 'billing':
        form3 = CompanyBillingForm(request.POST, instance=profile)
        form3.save(commit=False)
        form3.full_title = form3.cleaned_data['full_title']
        form3.billing_address = form3.cleaned_data['billing_address']
        form3.invoice_email = form3.cleaned_data['invoice_email']
        form3.bank_card = form3.cleaned_data['bank_card']
        form3.iban = form3.cleaned_data['iban']
        form3.vat_id = form3.cleaned_data['vat_id']
        form3.eu_vat_id = form3.cleaned_data['eu_vat_id']
        form3.swift = form3.cleaned_data['swift']
        form3.save()
        return HttpResponseRedirect('/employer/profile/#billing')

    if request.method == 'POST' and request.POST.get('form_name') == 'ml':
        form4 = MainLangForm(request.POST, instance=profile)
        if form4.is_valid():
            form4.save(commit=False)
            form4.main_lang_ui = form4.cleaned_data['main_lang_ui']
            try:
                activate(form4.main_lang_ui)
                print(form4.main_lang_ui)
            except:
                activate('en')
            form4.save()
            return redirect('employer:profile')

    return render(
        request,
        "employer/restricted/profile.html",
        {
            'profile': profile,
            'form': form,
            'form2': form2,
            'form3': form3,
            'form4': form4,
            'logo': logo,
            'invoices': invoices,
            'saved_resume': saved_resume
        })


@login_required()
@is_employer
def invoice(request, num=None):
    invoice = get_object_or_404(Invoice, pk=num)
    employer = invoice.invoiced_to.user
    if employer == request.user:
        return render(
            request,
            "employer/restricted/invoice.html",
            {'invoice': invoice})
    else:
        return HttpResponseRedirect('/employer/home/')


@login_required()
@is_employer
def cv_search(request):
    keywords = city = candidates = query = ''
    candidates = EmployeeProfile.objects.filter(is_active=True)
    categories_list = get_categories(lang=request.LANGUAGE_CODE)

    if request.method == 'POST':
        keywords = request.POST.get('keywords', '')
        city = request.POST.get('city', '')
        langs = request.POST.getlist('langs', '')
        category = request.POST.get('category', '')
        if city:
            candidates = candidates.filter(
                location_city__icontains=city)
        if keywords:
            keywords_list = keywords.split(' ')
            query = reduce(
                operator.and_,
                (Q(full_name__icontains=item) for item in keywords_list))
            candidates = candidates.filter(query)
        print(request.POST)
        if 'hu' in langs:
            candidates = candidates.filter(level_of_hu__gt=0)
        if 'ru' in langs:
            candidates = candidates.filter(level_of_ru__gt=0)
        if 'uk' in langs:
            candidates = candidates.filter(level_of_uk__gt=0)
        if 'en' in langs:
            candidates = candidates.filter(level_of_en__gt=0)
        if category:
            candidates = candidates.filter(job_category=category)
    return render(
        request,
        "employer/restricted/cv_search.html",
        {
            'cats_list': categories_list,
            'keywords': keywords,
            'city': city,
            'candidates': candidates,
        })


@login_required()
@is_employer
def delete_acc_request(request, pk):
    if pk:
        try:
            instance = get_object_or_404(
                EmployerProfile, pk=pk, user=request.user.id)
        except:
            raise Http404
    if instance:
        EmployerProfile.objects.filter(pk=instance.id).update(is_active=False)
        DeleteAccRequest.objects.create(
            profile=instance, canceled=False)
    return redirect('employer:profile')


@login_required()
@is_employer
def restore_acc_request(request, pk):
    if pk:
        try:
            instance = get_object_or_404(
                EmployerProfile, pk=pk, user=request.user.id)
        except:
            raise Http404
    if instance:
        EmployerProfile.objects.filter(pk=instance.id).update(is_active=True)
        DeleteAccRequest.objects.filter(
            profile=instance).update(canceled=True)
    return redirect('employer:profile')


@login_required()
@is_employer
def resume(request, pk=None):
    employee = get_object_or_404(EmployeeProfile, pk=pk)
    certs = Certificate.objects.filter(profile=employee, is_active=True)
    is_saved = SavedResume.objects.filter(
        user=request.user, resume_id=employee.id)
    my_profile = get_object_or_404(EmployerProfile, user=request.user)
    vacs_list = Vacancy.objects.filter(company=my_profile, status='act')
    is_invated = JobApplication.objects.filter(
        registered_user=employee, employer=my_profile)
    if request.POST:
        print(request.POST)
        message = request.POST.get('message', '')
        vac_id = request.POST.get('vacancy', '')
        print(vac_id)
        if vac_id:
            JobApplication.objects.create(
                fullname=employee.full_name,
                email=employee.email,
                registered_user=employee,
                message=message,
                current_vacancy=Vacancy.objects.get(pk=vac_id),
                employer=my_profile,
                status='new',
                is_invated=True,
            )
            return redirect('employer:resume', pk=pk)
    return render(request, "employer/restricted/resume.html", {
        'employee': employee,
        'certificates': certs,
        'is_saved': is_saved,
        'vacs_list': vacs_list,
        'is_invated': bool(is_invated)
    })


@login_required()
@is_employer
def save_resume(request, action='add', pk=None):
    resume = get_object_or_404(EmployeeProfile, pk=pk)
    if action == 'add':
        if pk and request.user.is_authenticated():
            employee_name = resume.full_name or resume.user
            SavedResume.objects.update_or_create(
                user=request.user, resume_id=pk, employee_name=employee_name)
    if action == 'del':
        if pk and request.user.is_authenticated():
            SavedResume.objects.filter(
                user=request.user, resume_id=pk).delete()
    return redirect('employer:resume', pk=pk)


@login_required()
@is_employer
def pricing(request):
    cv_search_prices = CvSearchPrice.objects.filter(is_active=True)
    job_add_prices = JobAddPrice.objects.filter(is_active=True)
    return render(
        request,
        "employer/restricted/pricing.html",
        {
            'cv_search_prices': cv_search_prices,
            'job_add_prices': job_add_prices
        }
    )

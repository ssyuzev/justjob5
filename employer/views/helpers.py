from employer.models.categories import JobCategory


def get_categories(lang='en'):
    categories = JobCategory.objects.all()
    categories_list = []
    for category in categories:
        if lang == 'ru':
            temp = (category.id, category.title_ru)
        elif lang == 'hu':
            temp = (category.id, category.title_hu)
        elif lang == 'uk':
            temp = (category.id, category.title_uk)
        else:
            temp = (category.id, category.title_en)
        categories_list.append(temp)
    return categories_list

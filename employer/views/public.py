from django.shortcuts import render, redirect
from django.utils.translation import activate

from employer.models.prices import JobAddPrice, CvSearchPrice


def home(request):
    user = request.user or ''
    if user.is_authenticated and user.profile.user_role == 'employer':
        return redirect('employer:cv_search')
    return render(request, "employer/home.html", {})


def search_cv(request):
    cv_search_prices = CvSearchPrice.objects.filter(is_active=True)
    if request.LANGUAGE_CODE == 'ru':
        activate('ru')
    elif request.LANGUAGE_CODE == 'hu':
        activate('hu')
    elif request.LANGUAGE_CODE == 'uk':
        activate('uk')
    else:
        activate('en')
    return render(
        request,
        "employer/search_cv.html",
        {
            'items': cv_search_prices
        }
    )


def post_job(request):
    job_add_prices = JobAddPrice.objects.filter(is_active=True)
    return render(
        request,
        "employer/post_job.html",
        {
            'items': job_add_prices
        }
    )


def recruitment(request):
    prefix = 'en'
    if request.LANGUAGE_CODE == 'ru':
        prefix = 'ru'
    elif request.LANGUAGE_CODE == 'hu':
        prefix = 'hu'
    elif request.LANGUAGE_CODE == 'uk':
        prefix = 'uk'
    else:
        prefix = 'en'
    template = "employer/restricted/recruitment/{}.html".format(prefix)
    return render(request, template, {})

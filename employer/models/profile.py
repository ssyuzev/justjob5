from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from employee.models import LANGS


class EmployerProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True)
    short_title = models.CharField(max_length=50, default='')
    company_phones = models.CharField(
        max_length=41, blank=True, null=True, default='')
    postal_address = models.CharField(
        max_length=150, blank=True, null=True, default='')
    about_company = models.TextField(
        max_length=1000, blank=True, null=True, default='')
    website = models.CharField(
        max_length=100, blank=True, null=True, default='')
    company_email = models.CharField(
        max_length=100, blank=True, null=True, default='')
    linkedin_url = models.CharField(
        max_length=150, blank=True, null=True, default='')
    logo = models.ImageField(
        upload_to='logo/', max_length=1280, blank=True, null=True)
    # contact person
    person_fullname = models.CharField(
        max_length=100, blank=True, null=True, default='')
    person_position = models.CharField(
        max_length=100, blank=True, null=True, default='')
    person_phone = models.CharField(
        max_length=41, blank=True, null=True, default='')
    person_email = models.CharField(
        max_length=100, blank=True, null=True, default='')
    # For billing
    full_title = models.CharField(
        max_length=150, blank=True, null=True, default='')
    billing_address = models.CharField(
        max_length=150, blank=True, null=True, default='')
    invoice_email = models.CharField(
        max_length=100, blank=True, null=True, default='')
    bank_card = models.CharField(
        max_length=12, blank=True, null=True, default='')
    iban = models.CharField(
        max_length=100, blank=True, null=True, default='')
    vat_id = models.CharField(
        max_length=100, blank=True, null=True, default='')
    eu_vat_id = models.CharField(
        max_length=100, blank=True, null=True, default='')
    swift = models.CharField(
        max_length=100, blank=True, null=True, default='')
    main_lang_ui = models.CharField(
        _('Main Interface language'),
        max_length=2, choices=LANGS, default='en')
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Company"
        verbose_name_plural = "Companies"

    def __str__(self):
        return "{} ({})".format(self.short_title, self.user)


class DeleteAccRequest(models.Model):
    profile = models.ForeignKey(
        EmployerProfile, on_delete=models.CASCADE, blank=True, null=True)
    uploaded_at = models.DateTimeField(auto_now=True)
    canceled = models.BooleanField(default=False)

    def __str__(self):
        return self.profile.user


class SavedResume(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    resume_id = models.PositiveIntegerField(null=True, blank=True)
    employee_name = models.CharField(max_length=157, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} / {}".format(self.user, self.employee_name)

from django.db import models
from django.utils.translation import ugettext_lazy as _


DURATION_LIST = (
    ('1m', _('1 month')),
    ('2m', _('2 months')),
    ('6m', _('6 months'))
)

POSITION_NUMS = (
    (0, _('Unlimited')),
    (1, ('1')),
    (3, ('3')),
    (5, ('5')),
    (6, ('6'))
)


class Price(models.Model):
    title = models.CharField(
        verbose_name=_('Title'), max_length=10, default='')
    price = models.IntegerField(verbose_name=_('Price, Ft.'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    duration = models.CharField(
        verbose_name=_('Duration'), choices=DURATION_LIST,
        default=DURATION_LIST[0], max_length=2)


class JobAddPrice(Price):
    highlight = models.BooleanField(
        verbose_name=_('Job highlight'), default=False)
    num_of_positions = models.IntegerField(
        verbose_name=_('No. of positions'), choices=POSITION_NUMS)

    def __str__(self):
        return self.title


class CvSearchPrice(Price):
    num_of_categories = models.IntegerField(
        verbose_name=_('No. of categories'), choices=POSITION_NUMS)

    def __str__(self):
        return self.title

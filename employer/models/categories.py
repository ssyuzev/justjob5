from django.db import models


class JobCategory(models.Model):
    title_en = models.CharField(
        max_length=77, blank=True, null=True, default='')
    title_hu = models.CharField(
        max_length=77, blank=True, null=True, default='')
    title_ru = models.CharField(
        max_length=77, blank=True, null=True, default='')
    title_uk = models.CharField(
        max_length=77, blank=True, null=True, default='')

    class Meta:
        verbose_name = "Job Category"
        verbose_name_plural = "Job Categories"

    def __str__(self):
        return self.title_en

from django.db import models

from .profile import EmployerProfile

INV_STATUS = (
    ('new', 'New'),
    ('paid', 'Paid'),
    ('cancel', 'Cancelled'))

INV_PAY_METHOD = (
    ('card', 'Credit Card'),
    ('bank', 'Account Transaction'))


class Invoice(models.Model):
    status = models.CharField(
        max_length=6, choices=INV_STATUS, default=INV_STATUS[0])
    pay_method = models.CharField(
        max_length=4, choices=INV_PAY_METHOD, default=INV_PAY_METHOD[0])
    pay_date = models.DateTimeField(blank=True, null=True)
    due_date = models.DateTimeField(blank=True, null=True)
    invoiced_to = models.ForeignKey(
        EmployerProfile, on_delete=models.CASCADE, blank=True, null=True)
    pay_to = models.CharField(max_length=156, blank=True, null=True)
    description = models.CharField(max_length=156, blank=True, null=True)
    amount = models.DecimalField(
        max_digits=5, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Invoice"
        verbose_name_plural = "Invoices"

    def __str__(self):
        return "#{} - {}".format(self.id, self.invoiced_to)

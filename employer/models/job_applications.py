from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .profile import EmployerProfile
from .vacancies import Vacancy

from employee.models import EmployeeProfile

APP_STATUSES = (
    ('new', _('New')),
    ('iv', _('Interviewed')),
    ('oe', _('Offer Extended')),
    ('hir', _('Hired')),
    ('arc', _('Archived')),
    ('dis', _('Discarded')))


class JobApplication(models.Model):
    fullname = models.CharField(
        _("Full Name"), max_length=254, blank=True, null=True)
    email = models.EmailField(
        _('Email'), max_length=104, blank=True, null=True)
    message = models.TextField(
        _('Message'), max_length=1000, blank=True, null=True)
    cv_file = models.FileField(
        _('Upload Resume'), upload_to='documents/', blank=True, null=True)
    current_vacancy = models.ForeignKey(
        Vacancy, null=True, blank=True, related_name='current_vacancy')
    registered_user = models.ForeignKey(
        EmployeeProfile, null=True, blank=True, related_name='candidate')
    employer = models.ForeignKey(
        EmployerProfile, null=True, blank=True, related_name='employer')
    # for candidates only is_deleted means moved_to_Archive
    is_deleted = models.BooleanField(
        _('Moved to archive by Candidate'), default=False)
    # for employers only
    status = models.CharField(
        max_length=3, choices=APP_STATUSES, default=APP_STATUSES[0])
    fullname_hu = models.CharField(
        max_length=254, blank=True, null=True)
    message_hu = models.TextField(
        max_length=1000, blank=True, null=True)
    is_viewed = models.BooleanField(default=False)
    # system
    discarded_by_candidate = models.BooleanField(default=False)
    discarded_by_employer = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_invated = models.BooleanField(default=False)

    def __str__(self):
        return "{} ({}) ".format(
            self.fullname, self.current_vacancy.title)


class Comment(models.Model):
    application = models.ForeignKey(
        JobApplication, blank=True, null=True, related_name='comments4app')
    message = models.TextField(max_length=300, default='')
    message_hu = models.TextField(
        max_length=400, blank=True, null=True, default='')
    created_by = models.ForeignKey(User, blank=True, null=True, default='')
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} ".format(self.application)

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .profile import EmployerProfile
from .categories import JobCategory


VAC_STATUS = (
    ('act', _('Active')),
    ('draft', _('Draft')),
    ('arch', _('Archived')),
    ('moder', _('On Moderation')))


CITIES = (
    ('bud', _('Budapest')),
    ('deb', _('Debrecen')))


LOCATION_RADIUS = (
    ('50', '< 50 km'),
    ('100', '< 100 km'),
    ('200', '< 200 km'),
    ('201', '> 200 km'))


JOB_TYPES = (
    ('ft', _('Full-Time')),
    ('pt', _('Part-Time')))


class Vacancy(models.Model):
    title = models.CharField(_('Title'), max_length=65)
    keywords = models.CharField(
        _('Keywords for search'), max_length=100, blank=True, null=True)
    phone_for_vac = models.CharField(
        _('Phone for this vacancy'), max_length=37, blank=True, null=True)
    email_for_vac = models.EmailField(
        _('E-mail for this vacancy'), max_length=100, blank=True, null=True)
    status = models.CharField(
        _('Vacancy Status'),
        max_length=5, choices=VAC_STATUS, default=VAC_STATUS[1])
    company = models.ForeignKey(
        EmployerProfile,
        verbose_name=_("Company"),
        on_delete=models.CASCADE, blank=True, null=True)
    updated_by = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published_at = models.DateField(_('Published Date'), blank=True, null=True)
    expired_at = models.DateField(_('Expired Date'), blank=True, null=True)
    short_desciption = models.CharField(
        _('Short Description'), max_length=100, blank=True, null=True)
    country = models.CharField(
        _('Country'), max_length=15, default='HU', blank=True)
    central_city = models.CharField(
        _('City'), max_length=3, choices=CITIES, default=CITIES[0])
    location = models.CharField(
        _('Location radius'),
        max_length=3, choices=LOCATION_RADIUS, blank=True, null=True)
    job_type = models.CharField(
        _('Job Type'), max_length=2, choices=JOB_TYPES, default=JOB_TYPES[0])
    category = models.ForeignKey(
        JobCategory, verbose_name=_("Job Category"), blank=True, null=True)
    salary = models.DecimalField(
        _('Salary rate/hour'),
        max_digits=3, decimal_places=0, blank=True, null=True)
    job_duties = models.TextField(
        _('Job Duties'), max_length=1000, default='', blank=True, null=True)
    job_demands = models.TextField(
        _('Job Demands'), max_length=1000, blank=True, null=True)
    job_conditions = models.TextField(
        _('Job Conditions'), max_length=1000, blank=True, null=True)
    job_description = models.TextField(
        _('Job Description'), max_length=1000, blank=True, null=True)
    work_hours = models.CharField(
        _('Work hours/week'), max_length=51, blank=True, null=True)
    is_hot = models.BooleanField(
        _('Hot vacancy'), default=False)
    is_need_hu = models.BooleanField(
        _('Knowledge of Hungarian is a must'), default=False)
    image = models.ImageField(
        _('Vacancy Image'),
        upload_to='vacancies/', max_length=1280, blank=True, null=True)

    class Meta:
        verbose_name = "Vacancy"
        verbose_name_plural = "Vacancies"

    def __str__(self):
        return self.title

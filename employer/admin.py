from django.contrib import admin

from .models.vacancies import Vacancy
from .models.profile import EmployerProfile, DeleteAccRequest, SavedResume
from .models.categories import JobCategory
from .models.job_applications import JobApplication, Comment
from .models.invoices import Invoice
from .models.prices import JobAddPrice, CvSearchPrice

class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'status', 'pay_method', 'pay_date', 'due_date', 'invoiced_to')
    list_filter = (
        'status', 'pay_method', 'pay_date', 'due_date', 'invoiced_to')
    search_fields = ('id', 'invoiced_to',)


class VacancyAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'company', 'status', 'created_at',
        'updated_at', 'published_at', 'expired_at')


class JobCategoryAdmin(admin.ModelAdmin):
    list_display = ('title_en', 'title_hu', 'title_ru', 'title_uk')


class JobApplicationAdmin(admin.ModelAdmin):
    list_display = (
        'current_vacancy', 'fullname', 'registered_user',
        'employer', 'created_at', 'discarded_by_candidate',
        'discarded_by_employer', 'status')


class CommentAdmin(admin.ModelAdmin):
    list_display = (
        'application', 'message', 'message_hu', 'created_by', 'created_at')


class DeleteAccRequestAdmin(admin.ModelAdmin):
    list_display = (
        'profile', 'uploaded_at', 'canceled')


admin.site.register(EmployerProfile)
admin.site.register(SavedResume)
admin.site.register(DeleteAccRequest, DeleteAccRequestAdmin)
admin.site.register(Vacancy, VacancyAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(JobCategory, JobCategoryAdmin)
admin.site.register(JobApplication, JobApplicationAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(JobAddPrice)
admin.site.register(CvSearchPrice)

from django import forms

from .models.profile import EmployerProfile
from .models.vacancies import Vacancy


class CompanyInfoForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='info')

    class Meta:
        model = EmployerProfile
        fields = [
            'short_title',
            'company_phones',
            'postal_address',
            'about_company',
            'website',
            'company_email',
            'linkedin_url',
            'logo',
            'form_name']


class CompanyPersonForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='person')

    class Meta:
        model = EmployerProfile
        fields = [
            'person_fullname',
            'person_position',
            'person_phone',
            'person_email',
            'form_name']


class CompanyBillingForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='billing')

    class Meta:
        model = EmployerProfile
        fields = [
            'full_title',
            'billing_address',
            'invoice_email',
            'bank_card',
            'iban',
            'vat_id',
            'eu_vat_id',
            'swift',
            'form_name']


class VacancyForm(forms.ModelForm):

    class Meta:
        model = Vacancy
        fields = [
            'title',
            'phone_for_vac',
            'email_for_vac',
            'job_duties',
            'job_demands',
            'job_conditions',
            'job_description',
            'category',
            'job_type',
            'central_city',
            'location',
            'salary',
            'work_hours',
            'is_hot',
            'is_need_hu',
        ]


class MainLangForm(forms.ModelForm):
    form_name = forms.CharField(widget=forms.HiddenInput(), initial='ml')

    class Meta:
        model = EmployerProfile
        fields = ['main_lang_ui']
